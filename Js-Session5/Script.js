let circleArray = [{
    score: -2,
    className: 'red-circle'
},{
	score: -1,
	className: 'violet-circle'
},{
    score: 3,
    className: 'green-circle'
},{
    score: 2,
    className: 'yellow-circle' 
},{
    score: 1,
    className: 'blue-circle'
}];
let timeLimit = 15;
let timerInstance;
let posX = 0;
let posY = 0;
let randomCircleIndex;
let circleGenerateInterval;
let circleObj;
let randomPositionValue;
let totalScore = 0;

$(document).ready(function() {
	$(".submit-btn").on("click",function() {

		$('.playarea').on('click', 'div', function(event) {
	        let className = event.target.className;
	        let scoreGained = getScores(className);
	        totalScore += scoreGained;
	        $('.displayscore').text(totalScore);
    	});

		if($('#Easy').is(':checked')){
			function startGameEasy(){
				timerInstance = setInterval(function(){
					timeLimit -= 1;
					$(".countdown").text(timeLimit);
					if(timeLimit === 0){
						endGame();
					}
				}, 1000);

			$(".playarea").css({position:"relative", margin: "auto", width: "600px", height: "480px", border: "1px solid #000000"});

			circleGenerateInterval = setInterval(function() {
				randomPositionValue = randomPositions($('#Easy'));
		        circleObj = randomCircles();
		        $('.playarea').html(`<div class="${circleObj.className}"></div>`);
		        $('.' + circleObj.className).css({position: 'absolute', left: randomPositionValue.x + 'px', top: randomPositionValue.y + 'px'});
		        }, 1500);
			}
			startGameEasy();
		}

		if($('#Medium').is(':checked')){
			function startGameMid(){
				timerInstance = setInterval(function(){
					timeLimit -= 1;
					$(".countdown").text(timeLimit);
					if(timeLimit === 0){
						endGame();
					}
				}, 1000);

				$(".playarea").css({position:"relative", margin: "auto", width: "800px", height: "600px", border: "1px solid #000000"});
				
				circleGenerateInterval = setInterval(function() {
					randomPositionValue = randomPositions($('#Easy'));
			        circleObj = randomCircles();
			        $('.playarea').html(`<div class="${circleObj.className}"></div>`);
			        $('.' + circleObj.className).css({position: 'absolute', left: randomPositionValue.x + 'px', top: randomPositionValue.y + 'px'});
		        }, 1500);
			}
			startGameMid();
		}

		if($('#Hard').is(':checked')){
			function startGameHard(){
				timerInstance = setInterval(function(){
					timeLimit -= 1;
					$(".countdown").text(timeLimit);
					if(timeLimit === 0){
						endGame();
					}
				}, 1000);

				$(".playarea").css({position:"relative",margin: "auto", width: "1024px", height: "768px", border: "1px solid #000000"});
				circleGenerateInterval = setInterval(function() {
					randomPositionValue = randomPositions($('#Hard'));
			        circleObj = randomCircles();
			        $('.playarea').html(`<div class="${circleObj.className}"></div>`);
			        $('.' + circleObj.className).css({position: 'absolute', left: randomPositionValue.x + 'px', top: randomPositionValue.y + 'px'});
		        }, 1500);
			}
			startGameHard();
		}

		function randomCircles(){
			randomCircleIndex =Math.floor(Math.random() * circleArray.length);
			return circleArray[randomCircleIndex];
		}

		function randomPositions(id){
			if(id.is($("#Easy"))){
				posX = Math.floor(Math.random() * 590);
				posY = Math.floor(Math.random() * 470);
			}

			if(id.is($("#Medium"))){
				posX = Math.floor(Math.random() * 790);
				posY = Math.floor(Math.random() * 590);
			}

			if(id.is($("#Hard"))){
				posX = Math.floor(Math.random() * 1010);
				posY = Math.floor(Math.random() * 760);
			}		 
			return{
				x : posX,
				y : posY
			}
		}

		function getScores(className){
			let arrValue = circleArray.filter(function(e){
				if(e.className == className){
					return true;
					//console.log(e,className);
				}
				return false;
			});
			if(arrValue.length > 0){
				return arrValue[0].score;
				//console.log(arrValue[0].score);
			}
			return 0;
		}

		function endGame(){
			clearInterval(timerInstance);
			clearInterval(circleGenerateInterval);
			$(".playarea").html(" ");
		} 
	});
});