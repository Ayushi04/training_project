//an array of every link on the page
var videoLinks = document.getElementsByTagName("a");


for (var i = 0; i < videoLinks.length; i++) {
	//current href stored in videolink
	var videoLink = videoLinks[i];
    var linkUrl = videoLink.getAttribute('href');

    //Thumbnail
    var thumbnailUrl = youtube.generateThumbnailUrl(linkUrl);

    //IMG element with attribute: src
    var thumbnailImg = document.createElement('img');
    thumbnailImg.setAttribute('src', thumbnailUrl);
	
	//thumbnailImg appends to parent videoLink
    videoLink.appendChild(thumbnailImg);
}