//Every image on the page (as a collection)
function assignment1(){
	document.getElementByTagName("img");
	//Output :
	/* HTMLCollection(11) [img, img, img.hide, img, img, img, img, img, img, img, img]
		0: img
		1: img
		2: img.hide
		3: img
		4: img
		5: img
		6: img
		7: img
		8: img
		9: img
		10: img
		length: 11
		__proto__: HTMLCollection */
}

//The MailChimp form at the bottom
function assignment2(){
	  document.getElementById("mc_embed_signup");
	  //Output :
	  /* <div id ="mc_embed_signup"></div>*/
}

//The three divs with GDI stats (as a collection)
function assignment3(){
	document.getElementsByClassName("stats");
	//Output :
	/* HTMLCollection(3) [div.stats, div.stats, div.stats]
		0: div.stats
		1: div.stats
		2: div.stats
		length: 3
		__proto__: HTMLCollection */
}

//The image header with the headline, "DON'T BE SHY DEVELOP IT"
function assignment4(){
	document.getElementsByClassName("opener")[0];
	//Output:
	/* <div class = "opener"></div>*/
}

//The navigation menu at the top
function assignment5(){
	document.querySelector(".navigation");
	//Output
	/* <div class = "navigation"></div> */
}

//The GDI logo in the upper-left
function assignment6(){
	 document.querySelector(".logo img");
	 //Output :
	 /*  <img src="/assets/gdi-logo-4ab2026ba5474725338d05eb1ba7b86a.png" alt="Gdi Logo">*/
}

//The media logos (TED, lifehacker, etc.) at the bottom (as a collection)
function assignment7(){
	document.querySelectorAll(".press-logos img");
	//Output: 
	// NodeList(5) [img, img, img, img, img]
}

//All the red dots on the US map (as a collection)
function assignment8(){
	document.querySelectorAll("circle");
	//Output : NodeList []
}

$(document).ready(function(){
	assignment1();
	assignment2();
	assignment3();
	assignment4();
	assignment5();
	assignment6();
	assignment7();
	assignment8();
});