function birthdays() {
	let currentDate = new Date();
	let birthdayArray = [];
	let i;
	function enterBirthdays() {
		birthdayArray[0] = new Date(1995,1,20);
		birthdayArray[1] = new Date(1996,5,16);
		birthdayArray[2] = new Date(1995,6,12);
		birthdayArray[3] = new Date(1995,4,1);
		birthdayArray[4] = new Date(1995,9,21);
		birthdayArray[5] = new Date(1994,9,21);
		
		console.log(birthdayArray);

		birthdayMonth(birthdayArray);
		isSunday(birthdayArray);
		sameDay(birthdayArray);
		nextBirthday(birthdayArray);
		oldestFriend(birthdayArray);
		addBirthday(birthdayArray);
		
	}

	function birthdayMonth(bArray) {
		for(i = 0; i < bArray.length; i++){
			if(currentDate.getMonth() == (bArray[i].getMonth() - 1)){
				console.log( (i+1) + "'s birthday is next month");
			} 
			else{
				console.log("No birthday next month for " + (i+1));
				continue;
			}
		}
	} 

	function isSunday(bArray){
		for(i = 0; i < bArray.length; i++){
			if(bArray[i].getDay() == 0){
				console.log( (i+1) + "'s birthday is on Sunday");
			}
		}
	}

	function sameDay(bArray) {
		for(i = 0; i < bArray.length; i++){
			let m = bArray[i].getMonth();
			let d = bArray[i].getDate();
			for( let j=i+1; j<bArray.length ; j++){
				if( (m == bArray[j].getMonth()) && (d == bArray[j].getDate())){
					console.log( "Friend " + i + " & " + j +"have their birthday on the same day");
				}	
			}
		}	
	}

	function nextBirthday(bArray){
		let net_t = [];
		for(i=0 ;i < bArray.length ; i++){
			net_t[i] = (currentDate.getTime()-(new Date(2020,bArray[i].getMonth(),bArray[i].getDate())).getTime());
			if(net_t[i]>0)
			{
				net_t[i]+=31557600000;
			}
			net_t[i]/=86400000;
		}
		console.log("Days left for birthdays are: " + net_t);
	}

	function oldestFriend(bArray){
		let milliSec = [];
		let old,j;
		for(i = 0; i < bArray.length; i++){
				milliSec[i] = currentDate.getTime() - bArray[i].getTime();
		}
		old = milliSec[0];		
		for(i = 0; i < bArray.length; i++){
			if (milliSec[i] >= old)
				j = i;
		}
		console.log("The oldest friend is "+(j+1));		
	}

	function addBirthday(bArray){
		let newDate = new Date(1993,1,1); 
		bArray.push(newDate);
		console.log("Pushed Birth Day: " + bArray[bArray.length - 1]);
	}

	return {
		enterBirthdays : enterBirthdays
	}
}

let birthdayCall = birthdays();
birthdayCall.enterBirthdays();