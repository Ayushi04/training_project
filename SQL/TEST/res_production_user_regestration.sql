-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: res_production
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_regestration`
--

DROP TABLE IF EXISTS `user_regestration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_regestration` (
  `S_No` int NOT NULL AUTO_INCREMENT,
  `First_Name` varchar(30) DEFAULT NULL,
  `Last_Name` varchar(10) DEFAULT NULL,
  `Gender` varchar(10) DEFAULT NULL,
  `DOB` varchar(15) DEFAULT NULL,
  `User_Type` varchar(15) DEFAULT NULL,
  `Address` varchar(40) DEFAULT NULL,
  `Phone_Number` int DEFAULT NULL,
  `Created_On` varchar(20) DEFAULT NULL,
  `Modified_On` varchar(20) DEFAULT NULL,
  `Created_By` varchar(20) DEFAULT NULL,
  `Modified_By` varchar(20) DEFAULT NULL,
  `Current_Status` binary(1) DEFAULT NULL,
  `User_Friend` varchar(30) DEFAULT NULL,
  `Books_Read` int DEFAULT NULL,
  PRIMARY KEY (`S_No`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_regestration`
--

LOCK TABLES `user_regestration` WRITE;
/*!40000 ALTER TABLE `user_regestration` DISABLE KEYS */;
INSERT INTO `user_regestration` VALUES (1,'Ayushi','Rai','Female','2-5-1994','Normal','S-332, Karol Bagh, New Delhi',24563218,'5-11-2010','9-4-2012','1','1',_binary '1','2',NULL),(2,'Aditi','Singh','Female','12-12-1994','Normal','D-32, GTB Nagar, New Delhi',24589637,'15-1-2017','19-4-2020','1','1',_binary '1','1',NULL);
/*!40000 ALTER TABLE `user_regestration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 13:50:37
