create schema RES_Production; 
use RES_Production; 
create table User_regestration(
	S_No integer primary key auto_increment, 
    First_Name varchar(30), 
    Last_Name varchar(10), 
    Gender varchar(10), 
    DOB varchar(15),
    User_Type varchar(15),
    Address varchar(40),
    Phone_Number integer,
    Created_On varchar(20),
    Modified_On varchar(20),
    Created_By varchar(20),
    Modified_By varchar(20),
    Status binary
    );
    alter table User_regestration rename column Status to Current_Status;
    desc User_regestration; 
    create table Book_Details (
		S_No integer primary key auto_increment, 
        Book_Title varchar(40),
        Book_Author varchar(30),
        Publisher varchar(30),
        Genre varchar(20),
        Year_of_Release varchar(14),
        Raring integer,
        Created_On varchar(20),
		Modified_On varchar(20),
		Created_By varchar(20),
		Modified_By varchar(20),
		Current_Status binary
    );
create table Author_Details (
	S_No integer primary key auto_increment, 
    Author_Name varchar(40),
	DOB varchar(15),
	Book_Description varchar(20),
	Created_On varchar(20),
	Modified_On varchar(20),
	Created_By varchar(20),
	Modified_By varchar(20),
	Current_Status binary
);  
create table Publisher_Details (
	S_No integer primary key auto_increment,
    Publisher_Name varchar(30),
    Established_On varchar(30),
    Address varchar(40),
    Publisher_Description varchar(30),
	Created_On varchar(20),
	Modified_On varchar(20),
	Created_By varchar(20),
	Modified_By varchar(20),
	Current_Status binary
);  




/* Add Author */
insert into Author_Details (Author_Name, DOB, Book_Description, Created_On, Modified_On, Created_By, Modified_By, Current_Status)
values ("J K Rowling", "10-4-1980", "Mystry", "7-10-2001", "7-10-2001", "1", "1", 1); 

/* Add 2 publishers */
insert into Publisher_Details (Publisher_Name, Established_On, Address, Publisher_Description, Created_On, Modified_On, Created_By, Modified_By, Current_Status)
 values ("Penguin", "10-2-1983", "North Avenue, A-32, CA", "Books of fantasy & friction", "17-10-2000", "17-10-2000", "1", "1", 1);

insert into Publisher_Details (Publisher_Name, Established_On, Address, Publisher_Description, Created_On, Modified_On, Created_By, Modified_By, Current_Status)
 values ("ABC Publications", "1-12-1990", "Chandi Mandir, D-12, India", "Books of Literature", "7-1-2000", "7-1-2000", "1", "1", 1);

 /*Add genre */
  
 insert into Book_Details (Book_Title, Book_Author, Publisher, Genre, Year_of_Release, Raring,  Created_On, Modified_On, Created_By, Modified_By, Current_Status)
 values ("Harry Potter and the cursed child", "J K Rowling", "1", "Fantasy", "2000", 8, "20-10-2000", "20-10-2000", "1", "1", 1);

 insert into Book_Details (Book_Title, Book_Author, Publisher, Genre, Year_of_Release, Raring,  Created_On, Modified_On, Created_By, Modified_By, Current_Status)
 values ("If tomorrow comes", "Sidney Sheldon", "1", "Action", "2005", 7, "20-10-2000", "20-10-2000", "1", "1", 1);

/* Add book */
 insert into Book_Details (Book_Title, Book_Author, Publisher, Genre, Year_of_Release, Raring,  Created_On, Modified_On, Created_By, Modified_By, Current_Status)
 values ("2 States", "Chetan Bhagat", "2", "Romance", "2009", 8, "13-7-2009", "13-7-2009", "1", "1", 1);

/* Add user  */
insert into User_regestration ( First_Name, Last_Name, Gender, DOB, User_Type, Address, Phone_Number, Created_On, Modified_On, Created_By, Modified_By, Current_Status
) values ("Ayushi", "Rai", "Female", "2-5-1994", "Normal", "S-332, Karol Bagh, New Delhi", 24563218, "5-11-2010", "9-4-2012", "1","1", 1);


/* Book 1 rented by user 1 */
create table Rent_Book (
	S_no integer primary key, 
    Person_Id integer, 
    Book_Id integer
); 
insert into Rent_Book values (1,1,1); 
/* Book 1 liked by user 1  */
create table Liked_Book (
	S_no integer primary key, 
    Person_Id integer, 
    Book_Id integer
); 
insert into Liked_Book values (1,1,1);


/* friends*/
alter table User_regestration add User_Friend varchar(30); 
insert into User_regestration ( First_Name, Last_Name, Gender, DOB, User_Type, Address, Phone_Number, Created_On, Modified_On, Created_By, Modified_By, Current_Status, User_Friend
) values ("Aditi", "Singh", "Female", "12-12-1994", "Normal", "D-32, GTB Nagar, New Delhi", 24589637, "15-1-2017", "19-4-2020", "1","1", 1,"1");
update User_regestration set User_Friend = "2" where S_No = 1;


/*wishlist */
create table wishlist (User_ID integer, Book_Id integer);
insert into wishlist values (1,2);

/* normal users */
select First_Name, Last_Name from User_regestration where User_Type = "Normal"; 

/* females */
select * from User_regestration where Gender = "Female";   

/*genre */
select genre from  Book_Details group by genre;  

/* rating > 4 */
alter table Book_Details rename column Raring to Rating;   
select Book_title, Rating from Book_Details where Rating > 4; 

/* heighest rating  */
select Book_title, MAX(Rating) as Heighest_Rating from Book_Details;  

/* lowest rating */
select Book_title, MIN(Rating) as Lowest_Rating from Book_Details; 

/* Author Name */
select Author_Name from Author_Details where Author_Name like "AR%"; 
  
/* Publisher Detail  */
select Publisher_Name,  Established_On from Publisher_Details where Established_On > "01-01-2012";  

/* highest rating */ 
select Book_title, Rating from Book_Details group by Rating;  

/* friends first name  */
select First_Name, User_Friend from  User_regestration
join User_regestration u1
join User_regestration u2
on u1.First_Name = u2.User_Friend; 

/* Release year */
select Year_of_Release from Book_Details where Year_of_Release between 2012 and 2018;

/*premium user */
select * from User_regestration where User_Type = "Premium" and sum(User_Friend) > 5; 

/* genre boooks */
select max(genre) from Book_Details; 

/* Author Name */
select Author_Name from  Author_Details;

/* liked books */ 
select * from Liked_Book
join User_regestration
on Liked_Book.Person_Id = User_regestration.First_Name
join Book_Details
on Liked_Book.Book_Id = Book_Details.Book_title
;  


/* wishlist books */
 
select * from  wishlist
join User_regestration
on  wishlist.User_Id = User_regestration.First_Name
join Book_Details
on  wishlist.Book_Id = Book_Details.Book_title
; 

/* premium users */

select First_Name, Last_name, Modified_By,
	CASE
    WHEN Modified_By > 10 THEN "Premium"
    ELSE "Not upgraded"
    END AS "Upgraded to"
from  User_regestration;  

/* read books */
alter table User_regestration add Books_Read integer;   
select First_Name, Last_name, Books_Read,
	CASE
    WHEN Books_Read > 5  and Modified_By > 5 THEN "Premium"
    ELSE "Not upgraded"
    END AS "Upgraded to"
from  User_regestration;